/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/31 21:36:17 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/01 01:16:50 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{
	int		i;
	int		j;
	int		tmp;

	i = 0;
	j = i + 1;
	while (i < size - 2)
	{
		if (i < j)
			i++;
		if (i > j)
		{
			tmp = tab[i];
			tab[i] = tab[j];
			tab[j] = tmp;
			i = 0;
		}
	}
}
#include <stdio.h>
int		main(void)
{
	int size;
	int tab[5] = {5,3,1,6,2};
	int i;
	i = 0;
	size = 5;
	ft_sort_int_tab(tab, size);
	while(i <= size - 1)
		{
			printf("%d\n",tab[i]);
			i++;
		}
	return(0);
}
