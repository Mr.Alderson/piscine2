/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prototype.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ateixeir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 16:46:35 by ateixeir          #+#    #+#             */
/*   Updated: 2019/09/08 22:29:28 by ateixeir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROT_H
# define PROT_H

int		check_x(int v, int grille[4][4], int x);
int		check_y(int v, int grille[4][4], int y);
int		check_indice_x(int grille[4][4], int *indice, int x, int y);
int		check_indice_y(int grille[4][4], int *indice, int y, int x);
int		rush(int grille[4][4], int *indice, int p);
int		check(char *str);
int		check_left(int grille[4][4], int *indice, int x);
int		check_right(int grille[4][4], int *indice, int x);
int		check_up(int grille[4][4], int *indice, int y);
int		check_down(int grille[4][4], int *indice, int y);

#endif
