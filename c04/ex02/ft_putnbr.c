/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/06 03:09:04 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/06 03:14:32 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}
void	ft_putnbr(int nb)
{
	int		i;

	i = 0;
	if (nb < 0)
	{
		if (nb == -2147483648)
		{
			write(1, "-2147483648", 11);
			return ;
		}
		else
			ft_putchar('-');
		nb = nb * -1;
	}
	while (nb / i >= 10)
		i = i * 10;
	while (i > 0)
	{
		ft_putchar((nb / i % 10) + 48);
		i = i / 10;
	}
}
