/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ateixeir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 20:52:07 by ateixeir          #+#    #+#             */
/*   Updated: 2019/09/08 22:30:59 by ateixeir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prot.h"

int		rush(int grille[4][4], int *indice, int p)
{
	int v;
	int x;
	int y;

	v = 1;
	x = p / 4;
	y = p % 4;
	if (p == 16)
		return (1);
	while (v <= 4)
	{
		if (check_x(v, grille, x) && check_y(v, grille, y))
		{
			grille[x][y] = v;
			if (check_indice_x(grille, indice, x, y) &&
					check_indice_y(grille, indice, y, x))
			{
				if (rush(grille, indice, p + 1))
					return (1);
			}
			grille[x][y] = 0;
		}
		v++;
	}
	return (0);
}
