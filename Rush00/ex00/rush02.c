/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thlefran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/01 04:01:20 by thlefran          #+#    #+#             */
/*   Updated: 2019/09/01 04:14:32 by thlefran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c);

void	first_line(int x)
{
	int		largeur;

	largeur = 1;
	while (largeur <= x)
	{
		if (largeur == 1)
			ft_putchar('A');
		else if (largeur == x)
			ft_putchar('A');
		else
			ft_putchar('B');
		largeur++;
	}
	ft_putchar('\n');
}

void	mid_line(int x)
{
	int		largeur;

	largeur = 1;
	while (largeur <= x)
	{
		if (largeur == 1)
			ft_putchar('B');
		else if (largeur == x)
			ft_putchar('B');
		else
			ft_putchar(' ');
		largeur++;
	}
	ft_putchar('\n');
}

void	last_line(int x)
{
	int		largeur;

	largeur = 1;
	while (largeur <= x)
	{
		if (largeur == 1)
			ft_putchar('C');
		else if (largeur == x)
			ft_putchar('C');
		else
			ft_putchar('B');
		largeur++;
	}
	ft_putchar('\n');
}

void	rush(int x, int y)
{
	int		hauteur;

	hauteur = 1;
	if (x <= 0 || y <= 0)
		return ;
	while (hauteur <= y)
	{
		if (hauteur == 1)
			first_line(x);
		else if (hauteur == y)
			last_line(x);
		else
			mid_line(x);
		hauteur++;
	}
}
