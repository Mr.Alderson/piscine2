/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   indice.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ateixeir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 20:51:11 by ateixeir          #+#    #+#             */
/*   Updated: 2019/09/08 22:26:46 by ateixeir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "prot.h"

int		check_left(int grille[4][4], int *indice, int x)
{
	int left;
	int k;
	int score;
	int y;

	score = 0;
	k = 0;
	y = 0;
	y = 0;
	left = indice[x + 8];
	while (y < 4)
	{
		if (grille[x][y] > k)
		{
			k = grille[x][y];
			score++;
		}
		y++;
	}
	if (score == left)
		return (1);
	return (0);
}

int		check_right(int grille[4][4], int *indice, int x)
{
	int right;
	int k;
	int score;
	int y;

	score = 0;
	k = 0;
	y = 3;
	right = indice[x + 12];
	while (y >= 0)
	{
		if (grille[x][y] > k)
		{
			k = grille[x][y];
			score++;
		}
		y--;
	}
	if (score == right)
		return (1);
	return (0);
}

int		check_up(int grille[4][4], int *indice, int y)
{
	int up;
	int k;
	int score;
	int x;

	score = 0;
	k = 0;
	x = 0;
	up = indice[y];
	while (x < 4)
	{
		if (grille[x][y] > k)
		{
			k = grille[x][y];
			score++;
		}
		x++;
	}
	if (score == up)
		return (1);
	return (0);
}

int		check_down(int grille[4][4], int *indice, int y)
{
	int down;
	int k;
	int score;
	int x;

	score = 0;
	k = 0;
	x = 3;
	down = indice[y + 4];
	while (x >= 0)
	{
		if (grille[x][y] > k)
		{
			k = grille[x][y];
			score++;
		}
		x--;
	}
	if (score == down)
		return (1);
	return (0);
}
