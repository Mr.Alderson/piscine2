/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ateixeir <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/08 19:13:50 by ateixeir          #+#    #+#             */
/*   Updated: 2019/09/08 22:30:33 by ateixeir         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "prot.h"

void		indice_init(int indice[16], char *str)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (i < 16)
	{
		indice[i] = str[j] - 48;
		j += 2;
		i++;
	}
}

void		grille_init(int grille[4][4])
{
	int x;
	int y;

	x = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			grille[x][y] = 0;
			y++;
		}
		x++;
	}
}

void		display(int grille[4][4])
{
	int x;
	int y;
	int w;

	x = 0;
	y = 0;
	w = 0;
	while (x < 4)
	{
		y = 0;
		while (y < 4)
		{
			w = grille[x][y] + 48;
			write(1, &w, 1);
			if (y != 3)
				write(1, " ", 1);
			y++;
		}
		write(1, "\n", 1);
		x++;
	}
}

int			main(int argc, char **argv)
{
	int indice[16];
	int grille[4][4];

	if (argc != 2)
	{
		write(1, "Error\n", 6);
		return (0);
	}
	if (check(argv[1]))
	{
		write(1, "Error\n", 6);
		return (0);
	}
	indice_init(indice, argv[1]);
	grille_init(grille);
	grille[0][0] = 0;
	rush(grille, indice, 0);
	if (grille[0][0] == 0)
	{
		write(1, "Error\n", 6);
		return (0);
	}
	display(grille);
	return (0);
}
